This is my submission for the OrderGroove project. The assignment was to create an UI where you can
search for items using the Etsy Public API.

I was told not to spend too much on the project. I spent a total of about 5-6 hours on the project. With
that in mind, there was a couple of functionality that I didn't get to include. Some are
  - Creating an 'items' page where you can get more specific data about an item clicked on
  - Creating a 'shops' page where you can see all items available for sale from a particular user
  - Adding the 'saved searches functionality'

Also, in the interest of time, I decided to focus more on the architecture and the backend, instead of delivering
a more fully polished UI (even though I think the UI looks pretty good).

Architecture-wise as well, although as I think its pretty solid, in the effort of time ,there were some shortcuts
used and there are some general improvements that would make this submission more better stable. Some include
  - Break out the search and filter component into two different components
  - The pagination section should ideally be it's own directive
  - Communication between controllers are done using events. Using a service with a somewhat persistent data storage
     such as localStorage would be a better architecture overall.

However, it is a coding exercise. My idea of exercises are to show a general idea of my coding style, rather than
deliver a fully production ready application. Comments are sparse as I think the code is generally straight-forward to
anyone reading it, but comments are included when I thought it would be necessary to.

### To Run
No backend components are needed. You should able to clone the app locally and then open up 'index.html' in your broswer.
No tests are included as well, because of time but all should work just well. This was written against a Chromse
browser but should work fine in any modern browser that's been updated within the last 6 months or so.