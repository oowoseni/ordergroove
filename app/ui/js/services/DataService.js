(function(){
    angular
        .module('app')
        .service('DataService', [
            '$http',
            function($http){
                function makeCall(data){
                    // Any modifications to 'data' pre-call should go here
                    var config = {
                        method: data.method || 'JSONP',
                        url: data.url += '.js?callback=JSON_CALLBACK',
                        params: data.params
                    };

                    if(data.params.includes) data.params.includes = data.params.includes.join(',');

                    return $http(config)
                        .then(function(returnObj){
                            if(returnObj.data.ok) return returnObj;
                            throw new Error('Server returned with a bad response');
                        });
                }

                function getSearchResults(data){
                    var config = {
                        url: '/listings/active',
                        params: data
                    };

                    config.params.includes = ['Images:100:0', 'User', 'Shop'];
                    if(!config.params.keywords) delete config.params.keywords;

                    return makeCall(config);
                }

                return {
                    getSearchResults: getSearchResults
                }
            }
        ]);
})();