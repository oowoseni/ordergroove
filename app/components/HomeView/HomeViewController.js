(function(){
    "use strict";

    angular
        .module('app')
        .controller('HomeViewController', [
            '$scope',
            function($scope){
                // This controller is really bare-bones as all functionality has been
                // encapsulated in other controllers. However, any functionality
                // specific to the home page would potentially live in this controller.
            }
        ]);
})();