(function(){
    "use strict";

    angular
        .module('app')
        .controller('ItemsListController', [
            '$scope',
            '$rootScope',
            'DataService',
            function($scope, $rootScope, DataService){
                $scope.items = [];

                $scope.sortBy = [
                    {
                        name: 'Recently Listed',
                        active: true,
                        sortKey: 'created'
                    },
                    {
                        name: 'Highest Price',
                        active: false,
                        sortKey: 'price'
                    },
                    {
                        name: 'Highest Views',
                        sortKey: 'score'
                    }
                ];

                $scope.maxSize = 5;
                $scope.active = 0;
                $scope.interval = 3000;
                $scope.perPage = 25;
                $scope.currentPage = 1;

                $scope.formData = {};

                function init(){
                    addListeners();
                    getSearchResults();
                }

                function getSort(){
                    var sorting = $scope.sortBy.find(function(obj){
                        return obj.active;
                    });

                    return sorting.sortKey;
                }

                function resetPagination(){
                    $scope.currentPage = 1;
                }

                function getSearchResults(){
                    var params = {
                        keywords: $rootScope.searchFormData.keywords,
                        page: $scope.currentPage,
                        sort_on: getSort()
                    };

                    if($rootScope.searchFormData && $rootScope.searchFormData){
                        params.min_price = $rootScope.searchFormData.prices.min_price;
                        params.max_price = $rootScope.searchFormData.prices.max_price;
                    }

                    return DataService.getSearchResults(params)
                        .then(function(results){
                            // API returns the same count no matter how much you filter
                            $scope.results = results.data.results;
                            $scope.totalItems = results.data.count;

                            return $scope.results;
                        });
                }

                function addListeners(){
                    // Listeners would be destroyed upon exit in a production env
                    $rootScope.$on('updateSearchResults', function(){
                        resetPagination();
                        getSearchResults();
                    });
                }

// ------------------------------------ SCOPE FUNCTIONS --------------------------------------
                
                $scope.switchSort = function switchSort(sortKey){
                    $scope.sortBy.forEach(function(obj){
                        obj.active = obj.sortKey === sortKey;
                    });

                    resetPagination();
                    getSearchResults();
                };

                $scope.paginationChange = function paginationChange(){
                    getSearchResults();
                };

                init();
            }
        ])
})();