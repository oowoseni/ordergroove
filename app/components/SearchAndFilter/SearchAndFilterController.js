(function(){
    "use strict";

    angular
        .module('app')
        .controller('SearchAndFilterController', [
            '$scope',
            '$rootScope',
            function($scope, $rootScope){
                // SAVING ALL RELEVANT SEARCH DATA IN ROOTSCOPE FOR SHARED ACCESS.
                // IDEALLY, A SERVICE OR EVEN POTENTIALLY LOCALSTORAGE SHOULD BE USED FOR THIS
                // BUT FOR BREVITY'S SAKE, THIS ALSO WORKS.

                $scope.formData = {};
                $scope.filters = {
                    Price: {
                        name: "Price",
                        options: [
                            {
                                text: "Under $5",
                                checked: false,
                                min: 0.00,
                                max: 4.99
                            },
                            {
                                text: "$5 - $25",
                                checked: false,
                                min: 5.00,
                                max: 24.99
                            },
                            {
                                text: "$25 - $50",
                                checked: false,
                                min: 25.00,
                                max: 49.99
                            },
                            {
                                text: "$50 - $100",
                                checked: false,
                                min: 49.99,
                                max: 99.99
                            },
                            {
                                text: "Over $100",
                                checked: false,
                                min: 100.00
                            }
                        ]
                    }
                };

                function init(){
                    updateRootScope();
                }

                function emitSearchEvent(){
                    $rootScope.$emit('updateSearchResults');
                }

                function updateRootScope(){
                    if(!$rootScope.searchFormData) $rootScope.searchFormData = {};
                    $rootScope.searchFormData = {
                        keywords: $scope.formData.search,
                        prices: getMinAndMaxPrices()
                    };
                }

                function clearPriceFilters(){
                    $scope.filters.Price.options.forEach(function(obj){
                        obj.checked = false;
                    });
                }

                function getMinAndMaxPrices(){
                    var min, max;
                    var checkedPriceFilters = $scope.filters.Price.options.filter(function(obj){
                        return obj.checked;
                    });

                    checkedPriceFilters.forEach(function(obj){
                        if(typeof min === undefined){
                            min = obj.min;
                        }
                        else{
                            min = min < obj.min ? min : obj.min;
                        }
                        
                        if(!obj.max){
                            max = obj.max;
                        }
                        else{
                            if(!max && obj.max){
                                max = obj.max;
                            }
                            else{
                                max = max < obj.max ? obj.max : max;
                            }
                        }
                    });

                    return {
                        min_price: min,
                        max_price: max
                    }
                }

// ------------------------------------ SCOPE FUNCTIONS --------------------------------------

                
                $scope.search = function search(){
                    updateRootScope();
                    emitSearchEvent();
                };

                $scope.clearSearchForm = function clearSearchForm(){
                    $scope.formData = {
                        search: ''
                    };

                    clearPriceFilters();
                    updateRootScope();
                    emitSearchEvent();
                };

                init();
            }
        ]);
})();