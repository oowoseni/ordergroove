(function(){
    'use strict';
    angular.module('app', [
            'ui.bootstrap',
            'ngRoute'
        ])
        // This should really belong in a config file or set as an env variable
        .constant('APIKEY', {
            url: 'https://openapi.etsy.com/v2',
            key: 'h1ehu4uqwe5x3waymlx4vtbf'
        })
        .config([
            '$routeProvider',
            '$httpProvider',
            function($routeProvider, $httpProvider){
                $httpProvider.interceptors.push('httpInterceptor');
                $routeProvider.when('/',
                    {
                        templateUrl: 'components/HomeView/HomeViewTemplate.html',
                        controller: 'HomeViewController'
                    })
                    .otherwise('/');
            }
        ])
        .factory('httpInterceptor', [
            '$q',
            'APIKEY',
            function($q, APIKEY){
                return {
                    request: function(config){
                        if(config.url.indexOf('callback=') === -1) return config;
                        config.url = APIKEY.url + config.url;
                        var opts = {
                            GET: function GET(){
                                if(!config.params) config.params = {};
                                config.params.api_key = APIKEY.key;
                                return config;
                            },
                            JSONP: function JSONP(){
                                return opts.GET();
                            }
                        };
                        return opts[config.method]();
                    }
                }
            }
        ])
})();

